var numero= 0;
var numero_save=0;
var ultimaOperacion='n';
var coma=false;


function C() {
    netejar();
    $("#numero").val(numero);
}
function CE() {
    netejar();
    $("#numero").val(numero);
    ultimaOperacion='n';
    numero_save=0;
}

function borrar() {
    var array=$("#numero").val().split("");
    numFinal=array.length;
    array[numFinal-1]="";
    array=array.join("");
    console.log=array;
    numero=parseFloat(array);
    $("#numero").val(numero);
}

function isZero(num) {
    if(num==0) {
        return true;
    } else {
        return false;
    }
}

function negatiu() {
    array=$("#numero").val().split("");
    
    if(isZero(numero)) {
        numero="0";
    } else if(array[0]!='-'){
        numero="-"+numero;
    } else {
        numero=numero*-1;
    }
    $("#numero").val(numero);
}

function afegirComa() {
    if(!coma) {
    if(isZero(numero)) {
        numero="0.";
    } else {
        numero=numero+".";
        coma=true;
    }
}
    
}
function netejar() {
    //var campoNumero=document.getElementById("numero");
    //campoNumero.value="";
    numero=0;
    coma=false;
}
function ferOperacio(simbol) {
    numero=$("#numero").val();
    if(ultimaOperacion=='n') {
        numero_save=parseFloat(numero);
    }
    ferUltimaOperacio();
    switch (simbol) {
        case '+':
            ultimaOperacion='+';
            netejar();
            break;
        case '-':
            ultimaOperacion='-';
            netejar();
            break;
        case '*':
            ultimaOperacion='*';
            netejar();
            break;
        case '/':
            ultimaOperacion='/';
            netejar();
            break;
        case '%':
            ultimaOperacion='%';
            netejar();
            break;

    }
    
}

function div1() {
    numero=1/numero;
    $("#numero").val(numero);
}

function arrelQuadrada() {
    var campoNumero=document.getElementById("numero");
    numero=Math.sqrt(campoNumero.value);
    $("#numero").val(numero);
}

function ferUltimaOperacio() {
    switch (ultimaOperacion) {
        case '+':
            numero_save=numero_save+parseFloat(numero);
            netejar();
            break;
        case '-':
            numero_save=numero_save-parseFloat(numero);
            netejar();
            break;
        case '*':
            numero_save=numero_save*parseFloat(numero);
            netejar();
            break;
        case '/':
            numero_save=numero_save/parseFloat(numero);
            netejar();
            break;
        case '%':
            numero_save=numero_save%parseFloat(numero);
            netejar();
            break;
    }
    $("#numero").val(numero_save);
}
function igual() {
    ferUltimaOperacio();
    $("#numero").val(numero_save);
    numero_save=0;
    ultimaOperacion='n';
}


function uno() {
    if(isZero(numero)) {
        numero="1";
    } else {
        numero=numero+"1";
    }
    $("#numero").val(numero);
}

function dos() {
    if(isZero(numero)) {
        numero="2";
    } else {
        numero=numero+"2";
    }
    $("#numero").val(numero);
}
function tres() {
    if(isZero(numero)) {
        numero="3";
    } else {
        numero=numero+"3";
    }
    $("#numero").val(numero);
}
function cuatro() {
    if(isZero(numero)) {
        numero="4";
    } else {
        numero=numero+"4";
    }
    $("#numero").val(numero);
}
function cinco() {
    if(isZero(numero)) {
        numero="5";
    } else {
        numero=numero+"5";
    }
    $("#numero").val(numero);
}

function seis() {
    if(isZero(numero)) {
        numero="6";
    } else {
        numero=numero+"6";
    }
    $("#numero").val(numero);
}
function siete() {
    if(isZero(numero)) {
        numero="7";
    } else {
        numero=numero+"7";
    }
    $("#numero").val(numero);
}
function ocho() {
    if(isZero(numero)) {
        numero="8";
    } else {
        numero=numero+"8";
    }
    $("#numero").val(numero);
}

function nueve() {
    if(isZero(numero)) {
        numero="9";
    } else {
        numero=numero+"9";
    }
    $("#numero").val(numero);
}

function zero() {
    if(isZero(numero)) {
        numero="0";
    } else {
        numero=numero+"0";
    }
    $("#numero").val(numero);
}
